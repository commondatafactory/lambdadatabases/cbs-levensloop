/*
	Transforming ItemsIn -> Items -> ItemsOut
	Where Items has column values ar integers to save memmory
	maps are needed to restore integers back to the actual values.
	those are generated and stored here.
*/

package main

type ModelMaps struct {
	Gemeente           MappedColumn
	Wijk               MappedColumn
	Buurt              MappedColumn
	Regiocode          MappedColumn
	BouwperiodeWoning  MappedColumn
	TypeEigendomWoning MappedColumn
}

var BitArrays map[string]fieldBitarrayMap

var Gemeente MappedColumn
var Wijk MappedColumn
var Buurt MappedColumn
var Regiocode MappedColumn
var BouwperiodeWoning MappedColumn
var TypeEigendomWoning MappedColumn

func clearBitArrays() {
	BitArrays = make(map[string]fieldBitarrayMap)
}

func init() {
	clearBitArrays()
	setUpRepeatedColumns()
}

func setUpRepeatedColumns() {
	Gemeente = NewReapeatedColumn("Gemeente")
	Wijk = NewReapeatedColumn("Wijk")
	Buurt = NewReapeatedColumn("Buurt")
	Regiocode = NewReapeatedColumn("Regiocode")
	BouwperiodeWoning = NewReapeatedColumn("Bouwperiode_woning")
	TypeEigendomWoning = NewReapeatedColumn("Type_eigendom_woning")

}

func CreateMapstore() ModelMaps {
	return ModelMaps{
		Gemeente,
		Wijk,
		Buurt,
		Regiocode,
		BouwperiodeWoning,
		TypeEigendomWoning,
	}
}

func LoadMapstore(m ModelMaps) {

	Gemeente = m.Gemeente
	Wijk = m.Wijk
	Buurt = m.Buurt
	Regiocode = m.Regiocode
	BouwperiodeWoning = m.BouwperiodeWoning
	TypeEigendomWoning = m.TypeEigendomWoning

	RegisteredColumns[Gemeente.Name] = Gemeente
	RegisteredColumns[Wijk.Name] = Wijk
	RegisteredColumns[Buurt.Name] = Buurt
	RegisteredColumns[Regiocode.Name] = Regiocode
	RegisteredColumns[BouwperiodeWoning.Name] = BouwperiodeWoning
	RegisteredColumns[TypeEigendomWoning.Name] = TypeEigendomWoning

}
