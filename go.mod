module lambdadb

go 1.15

require (
	github.com/JensRantil/go-csv v0.0.0-20200923162218-7ffda755f61b
	github.com/Workiva/go-datastructures v1.0.52
	github.com/cheggaaa/pb v1.0.29
	github.com/go-spatial/geom v0.0.0-20200810200216-9bf4204b30e9
	github.com/golang/geo v0.0.0-20200730024412-e86565bf3f35
	github.com/klauspost/compress v1.11.7 // indirect
	github.com/klauspost/pgzip v1.2.5
	github.com/lib/pq v1.10.7 // indirect
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.10.0
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
