/*
	model.go define the 'items' to store.
	All columns with getters and setters are defined here.

	ItemIn, represent rows from the Input data
	Item, the compact item stored in memmory
	ItemOut, defines how and which fields are exported out
	of the API. It is possible to ignore input columns

	Repeated values are stored in maps with int numbers
	as keys.  Optionally bitarrays are created for reapeated
	column values to do fast bit-wise filtering.

	A S2 geo index in created for lat, lon values.

	Unique values are stored as-is.

	The generated codes leaves room to create custom
	index functions yourself to create an API with an
	< 1 ms response time for your specific needs.

	This codebase solves: I need to have an API on this
	tabular dataset fast!
*/

package main

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"github.com/Workiva/go-datastructures/bitarray"
)

type registerGroupByFunc map[string]func(*Item) string
type registerGettersMap map[string]func(*Item) string
type registerReduce map[string]func(Items) map[string]string

type registerBitArray map[string]func(s string) (bitarray.BitArray, error)
type fieldBitarrayMap map[uint32]bitarray.BitArray

type ItemIn struct {
	Gemeente             string `json:"Gemeente"`
	Wijk                 string `json:"Wijk"`
	Buurt                string `json:"Buurt"`
	Regiocode            string `json:"Regiocode"`
	BouwperiodeWoning    string `json:"Bouwperiode_woning"`
	TypeEigendomWoning   string `json:"Type_eigendom_woning"`
	TotaalHuishoudens    string `json:"Totaal_huishoudens"`
	Hoofdbewoner01945    string `json:"hoofdbewoner_0_1945"`
	Hoofdbewoner19451955 string `json:"hoofdbewoner_1945_1955"`
	Hoofdbewoner19551970 string `json:"hoofdbewoner_1955_1970"`
}

type ItemOut struct {
	Gemeente             string `json:"Gemeente"`
	Wijk                 string `json:"Wijk"`
	Buurt                string `json:"Buurt"`
	Regiocode            string `json:"Regiocode"`
	BouwperiodeWoning    string `json:"Bouwperiode_woning"`
	TypeEigendomWoning   string `json:"Type_eigendom_woning"`
	TotaalHuishoudens    int64  `json:"Totaal_huishoudens"`
	Hoofdbewoner01945    int64  `json:"hoofdbewoner_0_1945"`
	Hoofdbewoner19451955 int64  `json:"hoofdbewoner_1945_1955"`
	Hoofdbewoner19551970 int64  `json:"hoofdbewoner_1955_1970"`
}

type Item struct {
	Label                int // internal index in ITEMS
	Gemeente             uint32
	Wijk                 uint32
	Buurt                uint32
	Regiocode            uint32
	BouwperiodeWoning    uint32
	TypeEigendomWoning   uint32
	TotaalHuishoudens    int64
	Hoofdbewoner01945    int64
	Hoofdbewoner19451955 int64
	Hoofdbewoner19551970 int64
}

func (i Item) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.Serialize())
}

// Shrink create smaller Item using uint32
func (i ItemIn) Shrink(label int) Item {

	Gemeente.Store(i.Gemeente)
	Wijk.Store(i.Wijk)
	Buurt.Store(i.Buurt)
	Regiocode.Store(i.Regiocode)
	BouwperiodeWoning.Store(i.BouwperiodeWoning)
	TypeEigendomWoning.Store(i.TypeEigendomWoning)

	totaalHuishoudens, err := strconv.ParseInt(strings.ReplaceAll(i.TotaalHuishoudens, " ", ""), 10, 64)
	if err != nil {
		totaalHuishoudens = -1
	}
	hoofdbewoner_0_1945, err := strconv.ParseInt(strings.ReplaceAll(i.Hoofdbewoner01945, " ", ""), 10, 64)
	if err != nil {
		hoofdbewoner_0_1945 = -1
	}

	hoofdbewoner_1945_1955, err := strconv.ParseInt(strings.ReplaceAll(i.Hoofdbewoner19551970, " ", ""), 10, 64)
	if err != nil {
		hoofdbewoner_1945_1955 = -1
	}

	hoofdbewoner_1955_1970, err := strconv.ParseInt(strings.ReplaceAll(i.Hoofdbewoner19451955, " ", ""), 10, 64)
	if err != nil {
		hoofdbewoner_1945_1955 = -1
	}

	return Item{

		label,

		Gemeente.GetIndex(i.Gemeente),
		Wijk.GetIndex(i.Wijk),
		Buurt.GetIndex(i.Buurt),
		Regiocode.GetIndex(i.Regiocode),
		BouwperiodeWoning.GetIndex(i.BouwperiodeWoning),
		TypeEigendomWoning.GetIndex(i.TypeEigendomWoning),
		totaalHuishoudens,
		hoofdbewoner_0_1945,
		hoofdbewoner_1945_1955,
		hoofdbewoner_1955_1970,
	}
}

// Store selected columns in seperate map[columnvalue]bitarray
// for fast item selection
func (i *Item) StoreBitArrayColumns() {
	SetBitArray("Regiocode", i.Regiocode, i.Label)
	SetBitArray("Bouwperiode_woning", i.BouwperiodeWoning, i.Label)
	SetBitArray("Type_eigendom_woning", i.TypeEigendomWoning, i.Label)

}

func (i Item) Serialize() ItemOut {
	return ItemOut{

		Gemeente.GetValue(i.Gemeente),
		Wijk.GetValue(i.Wijk),
		Buurt.GetValue(i.Buurt),
		Regiocode.GetValue(i.Regiocode),
		BouwperiodeWoning.GetValue(i.BouwperiodeWoning),
		TypeEigendomWoning.GetValue(i.TypeEigendomWoning),
		i.TotaalHuishoudens,
		i.Hoofdbewoner01945,
		i.Hoofdbewoner19451955,
		i.Hoofdbewoner19551970,
	}
}

func (i ItemIn) Columns() []string {
	return []string{

		"Gemeente",
		"Wijk",
		"Buurt",
		"Regiocode",
		"Bouwperiode_woning",
		"Type_eigendom_woning",
		"Totaal_huishoudens",
		"hoofdbewoner_0_1945",
		"hoofdbewoner_1945_1955",
		"hoofdbewoner_1955_1970",
	}
}

func (i ItemOut) Columns() []string {
	return []string{

		"Gemeente",
		"Wijk",
		"Buurt",
		"Regiocode",
		"Bouwperiode_woning",
		"Type_eigendom_woning",
		"Totaal_huishoudens",
		"hoofdbewoner_0_1945",
		"hoofdbewoner_1945_1955",
		"hoofdbewoner_1955_1970",
	}
}

func (i Item) Row() []string {

	return []string{

		Gemeente.GetValue(i.Gemeente),
		Wijk.GetValue(i.Wijk),
		Buurt.GetValue(i.Buurt),
		Regiocode.GetValue(i.Regiocode),
		BouwperiodeWoning.GetValue(i.BouwperiodeWoning),
		TypeEigendomWoning.GetValue(i.TypeEigendomWoning),
		fmt.Sprintf("%d", i.TotaalHuishoudens),
		fmt.Sprintf("%d", i.Hoofdbewoner01945),
		fmt.Sprintf("%d", i.Hoofdbewoner01945),
		fmt.Sprintf("%d", i.Hoofdbewoner19451955),
		fmt.Sprintf("%d", i.Hoofdbewoner19551970),
	}
}

func (i Item) GetIndex() string {
	return GettersRegiocode(&i)
}

func (i Item) GetGeometry() string {
	return ""
}

// contain filter Gemeente
func FilterGemeenteContains(i *Item, s string) bool {
	return strings.Contains(Gemeente.GetValue(i.Gemeente), s)
}

// startswith filter Gemeente
func FilterGemeenteStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Gemeente.GetValue(i.Gemeente), s)
}

// match filters Gemeente
func FilterGemeenteMatch(i *Item, s string) bool {
	return Gemeente.GetValue(i.Gemeente) == s
}

// getter Gemeente
func GettersGemeente(i *Item) string {
	return Gemeente.GetValue(i.Gemeente)
}

// contain filter Wijk
func FilterWijkContains(i *Item, s string) bool {
	return strings.Contains(Wijk.GetValue(i.Wijk), s)
}

// startswith filter Wijk
func FilterWijkStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Wijk.GetValue(i.Wijk), s)
}

// match filters Wijk
func FilterWijkMatch(i *Item, s string) bool {
	return Wijk.GetValue(i.Wijk) == s
}

// getter Wijk
func GettersWijk(i *Item) string {
	return Wijk.GetValue(i.Wijk)
}

// contain filter Buurt
func FilterBuurtContains(i *Item, s string) bool {
	return strings.Contains(Buurt.GetValue(i.Buurt), s)
}

// startswith filter Buurt
func FilterBuurtStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Buurt.GetValue(i.Buurt), s)
}

// match filters Buurt
func FilterBuurtMatch(i *Item, s string) bool {
	return Buurt.GetValue(i.Buurt) == s
}

// getter Buurt
func GettersBuurt(i *Item) string {
	return Buurt.GetValue(i.Buurt)
}

// contain filter Regiocode
func FilterRegiocodeContains(i *Item, s string) bool {
	return strings.Contains(Regiocode.GetValue(i.Regiocode), s)
}

// startswith filter Regiocode
func FilterRegiocodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Regiocode.GetValue(i.Regiocode), s)
}

// match filters Regiocode
func FilterRegiocodeMatch(i *Item, s string) bool {
	return Regiocode.GetValue(i.Regiocode) == s
}

// getter Regiocode
func GettersRegiocode(i *Item) string {
	return Regiocode.GetValue(i.Regiocode)
}

// contain filter BouwperiodeWoning
func FilterBouwperiodeWoningContains(i *Item, s string) bool {
	return strings.Contains(BouwperiodeWoning.GetValue(i.BouwperiodeWoning), s)
}

// startswith filter BouwperiodeWoning
func FilterBouwperiodeWoningStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(BouwperiodeWoning.GetValue(i.BouwperiodeWoning), s)
}

// match filters BouwperiodeWoning
func FilterBouwperiodeWoningMatch(i *Item, s string) bool {
	return BouwperiodeWoning.GetValue(i.BouwperiodeWoning) == s
}

// getter BouwperiodeWoning
func GettersBouwperiodeWoning(i *Item) string {
	return BouwperiodeWoning.GetValue(i.BouwperiodeWoning)
}

// contain filter TypeEigendomWoning
func FilterTypeEigendomWoningContains(i *Item, s string) bool {
	return strings.Contains(TypeEigendomWoning.GetValue(i.TypeEigendomWoning), s)
}

// startswith filter TypeEigendomWoning
func FilterTypeEigendomWoningStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(TypeEigendomWoning.GetValue(i.TypeEigendomWoning), s)
}

// match filters TypeEigendomWoning
func FilterTypeEigendomWoningMatch(i *Item, s string) bool {
	return TypeEigendomWoning.GetValue(i.TypeEigendomWoning) == s
}

// getter TypeEigendomWoning
func GettersTypeEigendomWoning(i *Item) string {
	return TypeEigendomWoning.GetValue(i.TypeEigendomWoning)
}

// contain filter TotaalHuishoudens
//func FilterTotaalHuishoudensContains(i *Item, s string) bool {
//	return strings.Contains(i.TotaalHuishoudens, s)
//}
//
//// startswith filter TotaalHuishoudens
//func FilterTotaalHuishoudensStartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.TotaalHuishoudens, s)
//}
//
//// match filters TotaalHuishoudens
//func FilterTotaalHuishoudensMatch(i *Item, s string) bool {
//	return i.TotaalHuishoudens == s
//}
//
//// getter TotaalHuishoudens
//func GettersTotaalHuishoudens(i *Item) string {
//	return i.TotaalHuishoudens
//}
//
//// contain filter Hoofdbewoner01945
//func FilterHoofdbewoner01945Contains(i *Item, s string) bool {
//	return strings.Contains(i.Hoofdbewoner01945, s)
//}
//
//// startswith filter Hoofdbewoner01945
//func FilterHoofdbewoner01945StartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.Hoofdbewoner01945, s)
//}
//
//// match filters Hoofdbewoner01945
//func FilterHoofdbewoner01945Match(i *Item, s string) bool {
//	return i.Hoofdbewoner01945 == s
//}
//
//// getter Hoofdbewoner01945
//func GettersHoofdbewoner01945(i *Item) string {
//	return i.Hoofdbewoner01945
//}
//
//// contain filter Hoofdbewoner19451955
//func FilterHoofdbewoner19451955Contains(i *Item, s string) bool {
//	return strings.Contains(i.Hoofdbewoner19451955, s)
//}
//
//// startswith filter Hoofdbewoner19451955
//func FilterHoofdbewoner19451955StartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.Hoofdbewoner19451955, s)
//}

// match filters Hoofdbewoner19451955
func FilterHoofdbewoner19451955Match(i *Item, s string) bool {
	si, err := strconv.ParseInt(s, 10, 64)

	if err != nil {
		return false
	}

	return i.Hoofdbewoner19451955 == si
}

//// getter Hoofdbewoner19451955
//func GettersHoofdbewoner19451955(i *Item) string {
//	return i.Hoofdbewoner19451955
//}
//
//// contain filter Hoofdbewoner19551970
//func FilterHoofdbewoner19551970Contains(i *Item, s string) bool {
//	return strings.Contains(i.Hoofdbewoner19551970, s)
//}
//
//// startswith filter Hoofdbewoner19551970
//func FilterHoofdbewoner19551970StartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.Hoofdbewoner19551970, s)
//}
//
//// match filters Hoofdbewoner19551970
//func FilterHoofdbewoner19551970Match(i *Item, s string) bool {
//	return i.Hoofdbewoner19551970 == s
//}
//
//// getter Hoofdbewoner19551970
//func GettersHoofdbewoner19551970(i *Item) string {
//	return i.Hoofdbewoner19551970
//}

/*
// contain filters
func FilterEkeyContains(i *Item, s string) bool {
	return strings.Contains(i.Ekey, s)
}


// startswith filters
func FilterEkeyStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Ekey, s)
}


// match filters
func FilterEkeyMatch(i *Item, s string) bool {
	return i.Ekey == s
}

// getters
func GettersEkey(i *Item) string {
	return i.Ekey
}
*/

// reduce functions
func reduceCount(items Items) map[string]string {
	result := make(map[string]string)
	result["count"] = strconv.Itoa(len(items))
	return result
}

type GroupedOperations struct {
	Funcs     registerFuncType
	GroupBy   registerGroupByFunc
	Getters   registerGettersMap
	Reduce    registerReduce
	BitArrays registerBitArray
}

var Operations GroupedOperations

var RegisterFuncMap registerFuncType
var RegisterGroupBy registerGroupByFunc
var RegisterGetters registerGettersMap
var RegisterReduce registerReduce
var RegisterBitArray registerBitArray

// ValidateRegsiters validate exposed columns do match filter names
func validateRegisters() {
	var i = ItemOut{}
	var filters = []string{"match", "contains", "startswith"}
	for _, c := range i.Columns() {
		for _, f := range filters {
			if _, ok := RegisterFuncMap[f+"-"+c]; !ok {
				fmt.Println(c + " is missing in RegisterMap")
			}
		}
	}
}

func init() {

	RegisterFuncMap = make(registerFuncType)
	RegisterGroupBy = make(registerGroupByFunc)
	RegisterGetters = make(registerGettersMap)
	RegisterReduce = make(registerReduce)

	// register search filter.
	//RegisterFuncMap["search"] = 'EDITYOURSELF'
	// example RegisterFuncMap["search"] = FilterEkeyStartsWith

	//RegisterFuncMap["value"] = 'EDITYOURSELF'
	// example RegisterGetters["value"] = GettersEkey

	// register filters

	//register filters for Gemeente
	RegisterFuncMap["match-Gemeente"] = FilterGemeenteMatch
	RegisterFuncMap["contains-Gemeente"] = FilterGemeenteContains
	RegisterFuncMap["startswith-Gemeente"] = FilterGemeenteStartsWith
	RegisterGetters["Gemeente"] = GettersGemeente
	RegisterGroupBy["Gemeente"] = GettersGemeente

	//register filters for Wijk
	RegisterFuncMap["match-Wijk"] = FilterWijkMatch
	RegisterFuncMap["contains-Wijk"] = FilterWijkContains
	RegisterFuncMap["startswith-Wijk"] = FilterWijkStartsWith
	RegisterGetters["Wijk"] = GettersWijk
	RegisterGroupBy["Wijk"] = GettersWijk

	//register filters for Buurt
	RegisterFuncMap["match-Buurt"] = FilterBuurtMatch
	RegisterFuncMap["contains-Buurt"] = FilterBuurtContains
	RegisterFuncMap["startswith-Buurt"] = FilterBuurtStartsWith
	RegisterGetters["Buurt"] = GettersBuurt
	RegisterGroupBy["Buurt"] = GettersBuurt

	//register filters for Regiocode
	RegisterFuncMap["match-Regiocode"] = FilterRegiocodeMatch
	RegisterFuncMap["contains-Regiocode"] = FilterRegiocodeContains
	RegisterFuncMap["startswith-Regiocode"] = FilterRegiocodeStartsWith
	RegisterGetters["Regiocode"] = GettersRegiocode
	RegisterGroupBy["Regiocode"] = GettersRegiocode

	//register filters for BouwperiodeWoning
	RegisterFuncMap["match-Bouwperiode_woning"] = FilterBouwperiodeWoningMatch
	RegisterFuncMap["contains-Bouwperiode_woning"] = FilterBouwperiodeWoningContains
	RegisterFuncMap["startswith-Bouwperiode_woning"] = FilterBouwperiodeWoningStartsWith
	RegisterGetters["Bouwperiode_woning"] = GettersBouwperiodeWoning
	RegisterGroupBy["Bouwperiode_woning"] = GettersBouwperiodeWoning

	//register filters for TypeEigendomWoning
	RegisterFuncMap["match-Type_eigendom_woning"] = FilterTypeEigendomWoningMatch
	RegisterFuncMap["contains-Type_eigendom_woning"] = FilterTypeEigendomWoningContains
	RegisterFuncMap["startswith-Type_eigendom_woning"] = FilterTypeEigendomWoningStartsWith
	RegisterGetters["Type_eigendom_woning"] = GettersTypeEigendomWoning
	RegisterGroupBy["Type_eigendom_woning"] = GettersTypeEigendomWoning

	//register filters for TotaalHuishoudens
	//RegisterFuncMap["match-Totaal_huishoudens"] = FilterTotaalHuishoudensMatch
	//RegisterFuncMap["contains-Totaal_huishoudens"] = FilterTotaalHuishoudensContains
	//RegisterFuncMap["startswith-Totaal_huishoudens"] = FilterTotaalHuishoudensStartsWith
	//RegisterGetters["Totaal_huishoudens"] = GettersTotaalHuishoudens
	//RegisterGroupBy["Totaal_huishoudens"] = GettersTotaalHuishoudens

	//register filters for Hoofdbewoner01945
	//RegisterFuncMap["match-hoofdbewoner_0_1945"] = FilterHoofdbewoner01945Match
	//RegisterFuncMap["contains-hoofdbewoner_0_1945"] = FilterHoofdbewoner01945Contains
	//RegisterFuncMap["startswith-hoofdbewoner_0_1945"] = FilterHoofdbewoner01945StartsWith
	//RegisterGetters["hoofdbewoner_0_1945"] = GettersHoofdbewoner01945
	//RegisterGroupBy["hoofdbewoner_0_1945"] = GettersHoofdbewoner01945

	//register filters for Hoofdbewoner19451955
	//RegisterFuncMap["match-hoofdbewoner_1945_1955"] = FilterHoofdbewoner19451955Match
	//RegisterFuncMap["contains-hoofdbewoner_1945_1955"] = FilterHoofdbewoner19451955Contains
	//RegisterFuncMap["startswith-hoofdbewoner_1945_1955"] = FilterHoofdbewoner19451955StartsWith
	//RegisterGetters["hoofdbewoner_1945_1955"] = GettersHoofdbewoner19451955
	//RegisterGroupBy["hoofdbewoner_1945_1955"] = GettersHoofdbewoner19451955

	//register filters for Hoofdbewoner19551970
	//RegisterFuncMap["match-hoofdbewoner_1955_1970"] = FilterHoofdbewoner19551970Match
	//RegisterFuncMap["contains-hoofdbewoner_1955_1970"] = FilterHoofdbewoner19551970Contains
	//RegisterFuncMap["startswith-hoofdbewoner_1955_1970"] = FilterHoofdbewoner19551970StartsWith
	//RegisterGetters["hoofdbewoner_1955_1970"] = GettersHoofdbewoner19551970
	//RegisterGroupBy["hoofdbewoner_1955_1970"] = GettersHoofdbewoner19551970

	validateRegisters()

	/*
		RegisterFuncMap["match-ekey"] = FilterEkeyMatch
		RegisterFuncMap["contains-ekey"] = FilterEkeyContains
		// register startswith filters
		RegisterFuncMap["startswith-ekey"] = FilterEkeyStartsWith
		// register getters
		RegisterGetters["ekey"] = GettersEkey
		// register groupby
		RegisterGroupBy["ekey"] = GettersEkey

	*/

	// register reduce functions
	RegisterReduce["count"] = reduceCount
}

type sortLookup map[string]func(int, int) bool

func createSort(items Items) sortLookup {

	sortFuncs := sortLookup{

		"Gemeente": func(i, j int) bool {
			return Gemeente.GetValue(items[i].Gemeente) < Gemeente.GetValue(items[j].Gemeente)
		},
		"-Gemeente": func(i, j int) bool {
			return Gemeente.GetValue(items[i].Gemeente) > Gemeente.GetValue(items[j].Gemeente)
		},

		"Wijk":  func(i, j int) bool { return Wijk.GetValue(items[i].Wijk) < Wijk.GetValue(items[j].Wijk) },
		"-Wijk": func(i, j int) bool { return Wijk.GetValue(items[i].Wijk) > Wijk.GetValue(items[j].Wijk) },

		"Buurt":  func(i, j int) bool { return Buurt.GetValue(items[i].Buurt) < Buurt.GetValue(items[j].Buurt) },
		"-Buurt": func(i, j int) bool { return Buurt.GetValue(items[i].Buurt) > Buurt.GetValue(items[j].Buurt) },

		"Regiocode": func(i, j int) bool {
			return Regiocode.GetValue(items[i].Regiocode) < Regiocode.GetValue(items[j].Regiocode)
		},
		"-Regiocode": func(i, j int) bool {
			return Regiocode.GetValue(items[i].Regiocode) > Regiocode.GetValue(items[j].Regiocode)
		},

		"Bouwperiode_woning": func(i, j int) bool {
			return BouwperiodeWoning.GetValue(items[i].BouwperiodeWoning) < BouwperiodeWoning.GetValue(items[j].BouwperiodeWoning)
		},
		"-Bouwperiode_woning": func(i, j int) bool {
			return BouwperiodeWoning.GetValue(items[i].BouwperiodeWoning) > BouwperiodeWoning.GetValue(items[j].BouwperiodeWoning)
		},

		"Type_eigendom_woning": func(i, j int) bool {
			return TypeEigendomWoning.GetValue(items[i].TypeEigendomWoning) < TypeEigendomWoning.GetValue(items[j].TypeEigendomWoning)
		},
		"-Type_eigendom_woning": func(i, j int) bool {
			return TypeEigendomWoning.GetValue(items[i].TypeEigendomWoning) > TypeEigendomWoning.GetValue(items[j].TypeEigendomWoning)
		},

		"Totaal_huishoudens":  func(i, j int) bool { return items[i].TotaalHuishoudens < items[j].TotaalHuishoudens },
		"-Totaal_huishoudens": func(i, j int) bool { return items[i].TotaalHuishoudens > items[j].TotaalHuishoudens },

		"hoofdbewoner_0_1945":  func(i, j int) bool { return items[i].Hoofdbewoner01945 < items[j].Hoofdbewoner01945 },
		"-hoofdbewoner_0_1945": func(i, j int) bool { return items[i].Hoofdbewoner01945 > items[j].Hoofdbewoner01945 },

		"hoofdbewoner_1945_1955":  func(i, j int) bool { return items[i].Hoofdbewoner19451955 < items[j].Hoofdbewoner19451955 },
		"-hoofdbewoner_1945_1955": func(i, j int) bool { return items[i].Hoofdbewoner19451955 > items[j].Hoofdbewoner19451955 },

		"hoofdbewoner_1955_1970":  func(i, j int) bool { return items[i].Hoofdbewoner19551970 < items[j].Hoofdbewoner19551970 },
		"-hoofdbewoner_1955_1970": func(i, j int) bool { return items[i].Hoofdbewoner19551970 > items[j].Hoofdbewoner19551970 },
	}
	return sortFuncs
}

func sortBy(items Items, sortingL []string) (Items, []string) {
	sortFuncs := createSort(items)

	for _, sortFuncName := range sortingL {
		sortFunc, ok := sortFuncs[sortFuncName]
		if ok {
			sort.Slice(items, sortFunc)
		}
	}

	// TODO must be nicer way
	keys := []string{}
	for key := range sortFuncs {
		keys = append(keys, key)
	}

	return items, keys
}
