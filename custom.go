package main

type registerCustomGroupByFunc map[string]func(*Item, ItemsGroupedBy)

var RegisterGroupByCustom registerCustomGroupByFunc

func init() {

	RegisterGroupByCustom = make(registerCustomGroupByFunc)

}

/*
func GettersToevoegingen(i *Item) string {
	return Postcode.GetValue(i.Postcode) + " " + Huisnummer.GetValue(i.Huisnummer)
}

func GetAdres(i *Item) string {
	adres := fmt.Sprintf("%s %s %s %s %s %s",
		Straat.GetValue(i.Straat),
		Huisnummer.GetValue(i.Huisnummer),
		Huisletter.GetValue(i.Huisletter),
		Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging),
		Postcode.GetValue(i.Postcode),
		Gemeentenaam.GetValue(i.Gemeentenaam))

	adres = strings.ReplaceAll(adres, "  ", " ")
	return adres
}
*/
