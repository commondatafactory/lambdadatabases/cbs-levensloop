package main

import (
	"fmt"
	//"github.com/prometheus/client_golang/prometheus"
	//"github.com/prometheus/client_golang/prometheus/promauto"
	"log"
	"net/http" //	"runtime/debug" "github.com/pkg/profile")
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Colors are fun, and can be used to note that this is joyfull and fun project.
const (
	//InfoColor    = "\033[1;34m%s\033[0m"
	//NoticeColor  = "\033[1;36m%s\033[0m"
	WarningColor = "\033[1;33m%s\033[0m"
	//ErrorColor   = "\033[1;31m%s\033[0m"
	//DebugColor   = "\033[0;36m%s\033[0m"

	InfoColorN    = "\033[1;34m%s\033[0m\n"
	NoticeColorN  = "\033[1;36m%s\033[0m\n"
	WarningColorN = "\033[1;33m%s\033[0m\n"
	//ErrorColorN   = "\033[1;31m%s\033[0m\n"
	//DebugColorN   = "\033[0;36m%s\033[0m\n"
)

func init() {
	itemChan = make(ItemsChannel, 1000)
}

func loadcsv(itemChan ItemsChannel) {
	log.Print("loading given csv")
	fmt.Println(SETTINGS.Get("delimiter"))
	err := importCSV(SETTINGS.Get("csv"), itemChan,
		false, true,
		SETTINGS.Get("delimiter"),
		SETTINGS.Get("null-delimiter"))

	if err != nil {
		log.Print(err)
	}

	// make sure channels are empty
	// add timeout there is no garantee ItemsChannel
	// is empty and you miss a few records
	timeout, _ := time.ParseDuration(SETTINGS.Get("channelwait"))
	time.Sleep(timeout)
	// S2CELLS.Sort()
	fmt.Println("csv imported")

	// Empty cache. should be made more generic
	cacheLock.Lock()
	defer cacheLock.Unlock()
	GroupByBodyCache = make(map[string]GroupByResult)
	GroupByHeaderCache = make(map[string]HeaderData)
}

func defaultSettings() {
	SETTINGS.Set("http_db_host", "0.0.0.0:8000", "host with port")
	SETTINGS.Set("SHAREDSECRET", "", "jwt shared secret")
	SETTINGS.Set("JWTENABLED", "n", "JWT enabled")

	SETTINGS.Set("CORS", "y", "CORS enabled")

	SETTINGS.Set("database", "n", "load a gzipped csv file on starup")
	SETTINGS.Set("csv", "", "load a gzipped csv file on starup")
	SETTINGS.Set("null-delimiter", "\\N", "null delimiter")
	SETTINGS.Set("delimiter", ",", "delimiter")

	SETTINGS.Set("mgmt", "n", "enable the management api's for lambdadb")
	SETTINGS.Set("debug", "n", "Add memory debug information during run")

	SETTINGS.Set("indexed", "n", "is the data indexed, for more information read the documenation?")
	SETTINGS.Set("strict-mode", "y", "strict mode does not allow ingestion of invalid items and will reject the batch")

	SETTINGS.Set("prometheus-monitoring", "n", "add promethues monitoring endpoint")
	SETTINGS.Set("STORAGEMETHOD", "bytesz", "Storagemethod available options are json, jsonz, bytes, bytesz")
	SETTINGS.Set("LOADATSTARTUP", "n", "Load data at startup. ('y', 'n')")

	SETTINGS.Set("readonly", "yes", "only allow read only funcions")
	SETTINGS.Set("debug", "no", "print memory usage")

	SETTINGS.Set("groupbycache", "yes", "use in memory cache")

	SETTINGS.Set("channelwait", "5s", "timeout")
	SETTINGS.Set("GOAT_KEY", "", "goat api key")

	// database settings.
	SETTINGS.Set("PGHOST", "127.0.0.1", "")
	SETTINGS.Set("PGDATABASE", "cdf", "")
	SETTINGS.Set("PGUSER", "cdf", "")
	SETTINGS.Set("PGPASSWORD", "insecure", "")
	SETTINGS.Set("PGPORT", "5433", "")
	SETTINGS.Set("PGSSLMODE", "disable", "")

	SETTINGS.Parse()
}

func main() {

	defaultSettings()

	go ItemChanWorker(itemChan)

	if SETTINGS.Get("csv") != "" {
		go loadcsv(itemChan)
	}

	if SETTINGS.Get("database") == "y" {
		fmt.Println("loading stuff from database")
		// go fillFromDB(itemChan)
	}

	if SETTINGS.Get("debug") == "y" {
		go runPrintMem()
	}

	if SETTINGS.Get("LOADATSTARTUP") == "y" {
		fmt.Println("start loading")
		go loadAtStart(SETTINGS.Get("STORAGEMETHOD"), FILENAME, SETTINGS.Get("indexed") == "y")
	}

	ipPort := SETTINGS.Get("http_db_host")

	mux := setupHandler()

	msg := fmt.Sprint(
		"starting server\nhost: ",
		ipPort,
	)
	fmt.Printf(InfoColorN, msg)
	log.Fatal(http.ListenAndServe(ipPort, mux))
}

func setupHandler() http.Handler {

	//JWTConfig := jwtConfig{
	//	Enabled:      false, //SETTINGS.Get("JWTENABLED") == "yes",
	//	SharedSecret: SETTINGS.Get("SHAREDSECRET"),
	//}

	Operations = GroupedOperations{
		Funcs:     RegisterFuncMap,
		GroupBy:   RegisterGroupBy,
		Getters:   RegisterGetters,
		Reduce:    RegisterReduce,
		BitArrays: RegisterBitArray,
	}

	searchRest := contextSearchRest(itemChan, Operations)
	typeAheadRest := contextTypeAheadRest(itemChan, Operations)
	listRest := contextListRest(itemChan, Operations)
	addRest := contextAddRest(itemChan, Operations)

	mux := http.NewServeMux()

	mux.HandleFunc("/search/", searchRest)
	mux.HandleFunc("/typeahead/", typeAheadRest)
	mux.HandleFunc("/list/", listRest)
	mux.HandleFunc("/help/", helpRest)

	// in docker we copy www2 to www
	mux.Handle("/", http.FileServer(http.Dir("./files/www")))
	mux.Handle("/dsm-search", http.FileServer(http.Dir("./files/www")))

	// local dev
	//mux.Handle("/", http.FileServer(http.Dir("./www2")))
	//mux.Handle("/dsm-search", http.FileServer(http.Dir("./www2")))

	if SETTINGS.Get("mgmt") == "y" {
		mux.HandleFunc("/mgmt/add/", addRest)
		mux.HandleFunc("/mgmt/rm/", rmRest)
		mux.HandleFunc("/mgmt/save/", saveRest)
		mux.HandleFunc("/mgmt/load/", loadRest)
	}

	if SETTINGS.Get("prometheus-monitoring") == "y" {
		mux.Handle("/metrics", promhttp.Handler())
	}

	fmt.Println("indexed: ", SETTINGS.Get("indexed"))

	cors := SETTINGS.Get("CORS") == "y"

	middleware := MIDDLEWARE(cors)

	msg := fmt.Sprint(
		"setup http handler:",
		" with:", len(ITEMS), "items ",
		"management api's: ", SETTINGS.Get("mgmt") == "y",
		" monitoring: ", SETTINGS.Get("prometheus-monitoring") == "yes", " CORS: ", cors)

	fmt.Printf(InfoColorN, msg)

	return middleware(mux)
}
